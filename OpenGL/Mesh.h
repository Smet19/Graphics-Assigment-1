#ifndef MESH_H
#define MESH_H

#include "StandardIncludes.h"
class Shader;

class Mesh
{
public:
	// Constructors / Destructors
	Mesh();
	virtual ~Mesh();

	// Methods
	void Create(Shader * _shader, vector<GLfloat> _vertexData);
	void Cleanup();
	void Render(glm::mat4 _wvp);
	glm::mat4 GetWorld() { return m_world; }
	void Translate(glm::vec3 _trans) { m_translation += _trans; }
	glm::vec3 GetTranslation() { return m_translation; }
	void SetRot(float _rot) { rot = _rot; }
	float GetRot() { return rot; }

private:
	Shader* m_shader;
	GLuint m_vertexBuffer; // GPU Buffer
	vector<GLfloat> m_vertexData;
	glm::mat4 m_world;
	glm::vec3 m_position;
	glm::vec3 m_translation;
	float rot;
};
#endif // MESH_H

