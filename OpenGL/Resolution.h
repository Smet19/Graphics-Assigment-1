#ifndef _RESOLUTION_H_
#define _RESOLUTION_H_

struct Resolution
{
	int m_width;
	int m_height;

	Resolution(int _width, int _height)
	{
		m_width = _width;
		m_height = _height;
	}
};

#endif //_RESOLUTION_H_

