#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "StandardIncludes.h"

class Texture
{
public:
	// Constructors / Destructors
	Texture();
	virtual ~Texture() { };

	// Accessors
	GLuint GetTexture() { return m_texture; }

	// Methods
	void LoadTexture(string _fileName);
	void Cleanup();

private:
	// Members
	int m_width;
	int m_height;
	int m_channels;
	GLuint m_texture;
};

#endif // !_TEXTURE_H_

