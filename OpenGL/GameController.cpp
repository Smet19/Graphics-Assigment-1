#include "GameController.h"
#include "WindowController.h"

GameController::GameController()
{
	m_shader = { };
	m_camera = { };
}

GameController::~GameController()
{
	for (auto* enemy : m_enemies)
	{
		delete enemy;
	}
}

void GameController::Initialize()
{
	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	M_ASSERT(glewInit() == GLEW_OK, "Failed to initialize GLEW");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.0f, 0.0f, 0.0, 0.0f);
	//glEnable(GL_CULL_FACE);

	m_camera = Camera(WindowController::GetInstance().GetResolution());
}

void GameController::RunGame()
{
	m_shader = Shader();
	m_shader.LoadShaders("SimpleVertexShader.vert", "SimpleFragmentShader.frag");

	m_player = Player();
	m_player.Create();

	for (int i = 0; i < 10; i++)
	{
		Enemy* enemy = new Enemy();
		m_enemies.push_back(enemy);
		enemy->Create();
	}

	GLFWwindow* win = WindowController::GetInstance().GetWindow();
	do
	{
		glClear(GL_COLOR_BUFFER_BIT);

		// UPDATE ONLY HERE
		m_player.Update();

		for (auto* enemy : m_enemies)
		{
			enemy->Update();
		}
		// UPDATE BEFORE THAT LINE

		glfwSwapBuffers(win);
		glfwPollEvents();

	} while (	glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS 
			&&	glfwWindowShouldClose(win) == 0);

	m_player.Cleanup();
	m_shader.Cleanup();
}