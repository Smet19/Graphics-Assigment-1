#include "Player.h"
#include "GameController.h"
#include "WindowController.h"


Player::Player()
{
	m_speed = 0.2f;
	m_position = { 0.0f, 0.0f, 0.0f };
	m_mesh = nullptr;
	m_vertexData = { };
}
Player::~Player()
{
	delete m_mesh;
}

void Player::Cleanup()
{
	if (m_mesh != nullptr)
	{
		m_mesh->Cleanup();
	}
}

glm::vec3 Player::GetTranslation() 
{ 
	if (m_mesh != nullptr)
		return m_mesh->GetTranslation();
	else
		return glm::vec3();
}

void Player::Move()
{
	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	glm::vec3 translation = { 0.0f, 0.0f, 0.0f };

	if (glfwGetKey(window, GLFW_KEY_W))
		translation += glm::vec3(0.0f, m_speed, 0.0f);

	if (glfwGetKey(window, GLFW_KEY_A))
		translation += glm::vec3(-m_speed, 0.0f, 0.0f);

	if (glfwGetKey(window, GLFW_KEY_S))
		translation += glm::vec3(0.0f, -m_speed, 0.0f);

	if (glfwGetKey(window, GLFW_KEY_D))
		translation += glm::vec3(m_speed, 0.0f, 0.0f);

	m_mesh->Translate(translation);
}

void Player::Create()
{
	m_vertexData = {
		/* Position */			/* RGBA Color*/
		-1.0f, -1.0f, 0.0f,		1.0f, 0.0f, 0.0f, 1.0f, // Red
		1.0f, -1.0f, 0.0f,		1.0f, 0.0f, 0.0f, 1.0f, // Red
		0.0f, 1.0f, 0.0f,		1.0f, 0.0f, 0.0f, 1.0f	// Red
	};

	m_mesh = new Mesh();
	m_mesh->Create(GameController::GetInstance().GetShader(), m_vertexData);
}

void Player::Update()
{
	Camera* camera = GameController::GetInstance().GetCamera();
	Move();
	m_mesh->Render(camera->GetProjection() * camera->GetView());
}