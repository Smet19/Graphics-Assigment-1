#include "Mesh.h"
#include "Shader.h"

Mesh::Mesh()
{
	m_shader = nullptr;
	m_vertexBuffer = 0;
	m_world = glm::mat4(1.0f);
	m_position = { 0.0f, 0.0f, 0.0f };
	m_translation = { 0.0f, 0.0f, 0.0f };
	rot = 0;
}

Mesh::~Mesh()
{
}

void Mesh::Create(Shader* _shader, vector<GLfloat> _vertexData)
{
	m_shader = _shader;

	m_vertexData = _vertexData;

	glGenBuffers(1, &m_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, m_vertexData.size() * sizeof(float), m_vertexData.data(), GL_STATIC_DRAW);
}

void Mesh::Cleanup()
{
	glDeleteBuffers(1, &m_vertexBuffer);
}

void Mesh::Render(glm::mat4 _wvp)
{
	glUseProgram(m_shader->GetProgramID());
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

	// 1st attribute : vertices
	glEnableVertexAttribArray(m_shader->GetAttrVertices());
	glVertexAttribPointer(	m_shader->GetAttrVertices(),
							3,
							GL_FLOAT,
							GL_FALSE,
							7 * sizeof(float),
							(void*)0);

	// 2nd attribute : colors
	glEnableVertexAttribArray(m_shader->GetAttrColors());
	glVertexAttribPointer(	m_shader->GetAttrColors(),
							4,
							GL_FLOAT,
							GL_FALSE,
							7 * sizeof(float),
							(void*)(3 * sizeof(float)));

	glm::mat4 transform = glm::translate(m_world, m_translation);
	transform = glm::rotate(transform, rot, glm::vec3(0.0f, 0.0f, 1.0f));
	_wvp *= transform;

	glUniformMatrix4fv(m_shader->GetAttrWVP(), 1, GL_FALSE, &_wvp[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, m_vertexData.size() / 7); // Draw the triangle
	glDisableVertexAttribArray(m_shader->GetAttrColors());
	glDisableVertexAttribArray(m_shader->GetAttrVertices());
}