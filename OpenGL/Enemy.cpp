#include "Enemy.h"
#include "GameController.h"
#include "WindowController.h"

Enemy::Enemy()
{
	m_speed = 0.1f;
	m_position = { 0.0f, 0.0f, 0.0f };
	m_mesh = nullptr;
	m_tagged = false;
	m_vertexData = { };
}
Enemy::~Enemy()
{
	delete m_mesh;
}

void Enemy::Cleanup()
{
	if (m_mesh != nullptr)
	{
		m_mesh->Cleanup();
	}
}

glm::vec3 Enemy::GetTranslation()
{
	if (m_mesh != nullptr)
		return m_mesh->GetTranslation();
	else
		return glm::vec3();
}

void Enemy::Teleport(glm::vec3 _position)
{
	glm::vec3 translation = _position - m_position;

	m_mesh->Translate(translation);

	m_position = GetTranslation();
}

void Enemy::Move(glm::vec3 _position)
{
	glm::vec3 translation = _position * m_speed;

	m_mesh->Translate(translation);

	m_position = GetTranslation();

	Rotate();
}

void Enemy::Create()
{
	m_vertexData = {
		/* Position */			/* RGBA Color*/
		-1.0f, -1.0f, 0.0f,		0.0f, 1.0f, 0.0f, 1.0f, // Green
		1.0f, -1.0f, 0.0f,		0.0f, 1.0f, 0.0f, 1.0f, // Green
		0.0f, 1.0f, 0.0f,		0.0f, 1.0f, 0.0f, 1.0f // Green
	};

	m_mesh = new Mesh();
	m_mesh->Create(GameController::GetInstance().GetShader(), m_vertexData);

	random_device device;
	mt19937 generator(device());
	uniform_int_distribution<int> x_dist(2, 10);
	uniform_int_distribution<int> y_dist(2, 10);
	uniform_int_distribution<int> posneg(0, 1);
	int mult1 = posneg(generator) ? 1 : -1;
	int mult2 = posneg(generator) ? 1 : -1;
	Teleport({(float)x_dist(generator) * mult1, (float)y_dist(generator) * mult2, 0.0f });
}

void Enemy::Tag()
{
	m_tagged = true;

	m_vertexData = 
	{
		/* Position */			/* RGBA Color*/
		-1.0f, -1.0f, 0.0f,		0.0f, 0.0f, 1.0f, 1.0f, // Blue
		1.0f, -1.0f, 0.0f,		0.0f, 0.0f, 1.0f, 1.0f, // Blue
		0.0f, 1.0f, 0.0f,		0.0f, 0.0f, 1.0f, 1.0f // Blue
	};

	glm::vec3 pos = GetTranslation();
	m_mesh->Cleanup();
	delete m_mesh;

	m_mesh = new Mesh();
	m_mesh->Create(GameController::GetInstance().GetShader(), m_vertexData);
	m_mesh->Translate(pos);

	Camera* camera = GameController::GetInstance().GetCamera();

	m_mesh->Render(camera->GetProjection() * camera->GetView());
}

void Enemy::Rotate()
{
	glm::vec3 pTrans = GameController::GetInstance().GetPlayer()->GetTranslation();
	glm::vec3 dif = glm::vec3(glm::normalize(pTrans - GetTranslation()));
	float rot = 0.0f;

	rot = glm::dot(dif, glm::vec3(0.0f, 1.0f, 0.0f));
	rot = glm::acos(rot);

	if (dif.x > 0)
		m_mesh->SetRot(-rot);
	else
		m_mesh->SetRot(rot);
}

void Enemy::Update()
{
	Camera* camera = GameController::GetInstance().GetCamera();
	
	// TODO Add logic of runnig from player triangle
	glm::vec3 pTrans = GameController::GetInstance().GetPlayer()->GetTranslation();
	float dist = glm::distance(GetTranslation(), pTrans);
	glm::vec3 diff = glm::normalize(pTrans - GetTranslation());

	if (dist < 10)
	{
		if (dist <= 1)
		{
			Tag();
		}
		Move(glm::normalize(-diff));
	}
	else if (dist > 11)
	{
		Move(diff);
	}

	m_mesh->Render(camera->GetProjection() * camera->GetView());
}