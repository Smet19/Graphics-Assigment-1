#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "StandardIncludes.h"
#include "Mesh.h"

class Enemy
{
public:
	Enemy();
	virtual ~Enemy();
	void Create();
	void Update();
	void Move(glm::vec3 _position);
	void Teleport(glm::vec3 _position);
	bool IsTagged() { return m_tagged; }
	void Tag();
	void Cleanup();
	void Rotate();
	Mesh* GetMesh() { return m_mesh; }
	glm::vec3 GetTranslation();


private:
	bool m_tagged;
	float m_speed;
	glm::vec3 m_position;
	glm::vec3 m_color;
	Mesh* m_mesh;
	vector<GLfloat> m_vertexData;
};

#endif //! _ENEMY_H_