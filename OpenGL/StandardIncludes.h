#ifndef STANDARD_INCLUDES_H
#define STANDARD_INCLUDES_H

#define GLM_ENABLE_EXPERIMENTAL

// Include standard header
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <random>

// Windows specific includes and defines
#ifdef _WIN32
#include <Windows.h>
#define M_ASSERT(_cond, _msg) \
	if (!(_cond)) { OutputDebugStringA(_msg); glfwTerminate(); std::abort(); }
#endif

// OpenGL / Helpder headers
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Resolution.h"
#include "Singleton.h"

using namespace std;

#endif //STANDARD_INCLUDES_H