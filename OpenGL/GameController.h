#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include "StandardIncludes.h"
#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"
#include "Player.h"
#include "Enemy.h"
#include <list>

class GameController : public Singleton<GameController>
{
public:
	// Constructors / Destructors
	GameController();
	virtual ~GameController();

	// Methods
	void Initialize();
	void RunGame();
	Shader* GetShader() { return &m_shader; }
	Camera* GetCamera() { return &m_camera; }
	Player* GetPlayer() { return &m_player; }
	void GenerateEnemies();
	void DeleteEnemies();

private:
	Shader m_shader;
	Camera m_camera;
	Player m_player;
	std::list<Enemy*> m_enemies;
};

#endif // GAMECONTROLLER_H

