#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "StandardIncludes.h"
#include "Mesh.h"

class Player
{
public:
	Player();
	virtual ~Player();
	void Create();
	void Update();
	void Move();
	Mesh* GetMesh() { return m_mesh; }
	void Cleanup();
	glm::vec3 GetTranslation();


private:
	float m_speed;
	glm::vec3 m_position;
	glm::vec3 m_color;
	Mesh* m_mesh;
	vector<GLfloat> m_vertexData;
};

#endif //! _PLAYER_H_